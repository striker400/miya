package com.xiaoheng999.miya.service;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.xiaoheng999.miya.domain.User;
import com.xiaoheng999.miya.spingmvc.model.Student;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:config/core/spring-common.xml"})  
public class TestUserService {
	private static Logger logger = LoggerFactory.getLogger(TestUserService.class); 
	@Resource  
    private UserService userService;
	
	@Test
	public void test1(){
		logger.info("userservice:"+ (userService==null));
		Method [] methods = userService.getClass().getMethods();
		for (int i = 0; i < methods.length; i++) {
			logger.info("userservice methods:"+methods[i].getName() );
		}
		User user = userService.selectByPrimaryKey(1);
		logger.info("用户名："+user.getUserName());
	}
	
	@Test
	public void testEL(){
		//ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		Student stu = new Student();
		System.out.println("---->NAME:"+stu.getName());
	}
}

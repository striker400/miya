package com.xiaoheng999.miya.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    @RequestMapping("/index")
    public String hello(Model model) {
        model.addAttribute("greeting", "Hello Spring MVC");

        return "index";

    }
    
    @RequestMapping("/somePath")
    public String requestResponseExample(HttpServletRequest request,
            HttpServletResponse reponses, Model model) {
        // Todo something here
  
        return "someView";
    }
    
    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public String authorInfo(Model model) {
       // Do somethong here
 
        return "redirect:/login?main";
    }
    
    
    @RequestMapping("/user")
    public String userInfo(Model model, @RequestParam(value = "name", defaultValue = "Guest") String name) {

        model.addAttribute("name", name);

        if ("admin".equals(name)) {
            model.addAttribute("email", "admin@yiibai.com");
        } else {
            model.addAttribute("email", "Not set");
        }
        return "pages/userInfo";
    }
}

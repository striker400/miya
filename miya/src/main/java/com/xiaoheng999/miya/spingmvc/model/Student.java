package com.xiaoheng999.miya.spingmvc.model;

import org.springframework.beans.factory.annotation.Value;

public class Student {
	//@Range(min = 1, max = 150) 
    private Integer age;
	//@NotEmpty
    @Value("李四")
    private String name;
    @Value("22")
    private Integer id;

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}

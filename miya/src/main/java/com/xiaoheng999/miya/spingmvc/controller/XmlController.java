package com.xiaoheng999.miya.spingmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiaoheng999.miya.spingmvc.model.XmlModel;


@Controller
@RequestMapping("/xml")
public class XmlController {

   @RequestMapping(value="{name}", method = RequestMethod.GET)
   public @ResponseBody XmlModel getUser(@PathVariable String name) {

	   XmlModel user = new XmlModel();

      user.setName(name);
      user.setId(1);
      return user;
   }
}

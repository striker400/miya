package com.xiaoheng999.miya.spingmvc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class PDFController {
	
    @RequestMapping(value = "/pdfcreate", method = RequestMethod.GET)
    public ModelAndView student() {
    	Map<String, String> userData = new HashMap<String, String>();
		userData.put("100", "Xiao.Lu");
		userData.put("102", "User 102");
		userData.put("301", "User 301");
		userData.put("400", "User 400");
		return new ModelAndView("UserSummary", "userData", userData);
    }

}
